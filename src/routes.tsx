import React from "react"
import { renderRoutes } from "react-router-config"
import { usersRoutes } from "./features/users";
import { NotFoundPage } from "./features/common";

const routes = [
  ...usersRoutes(),
  { component: NotFoundPage },
]

export const Routes: React.FC = () => (
  <React.Fragment>
    {renderRoutes(routes)}
  </React.Fragment>
)
