import React from 'react'

export interface AccountLoaderProps {
  children: React.ReactElement
}

export const AccountLoader: React.FC<AccountLoaderProps> = ({ children }) => {
  return children
}
