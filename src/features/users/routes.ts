import { User } from "./pages/User"
import { RouteConfig } from "react-router-config";

export function usersRoutes (): RouteConfig[] {
  return [
    {
      path: "/user",
      exact: true,
      component: User,
    },
  ]
}
