import React from "react"
import { hot } from "react-hot-loader"

import { Routes } from "./routes"
import { GlobalStyles } from "./lib/styles/globalStyles";
import  "./lib/styles/normalize";
import { AccountLoader } from "./features/common"

export const App = hot(module)(() => (
  <React.Fragment>
    <GlobalStyles/>
    <AccountLoader>
      <Routes/>
    </AccountLoader>
  </React.Fragment>
))
