import { createBrowserHistory } from "history"

//for example
// import React from "react"
// import { Redirect } from "react-router-dom"

// export const redirectTo = (path: string, key?: string) => () => ( <Redirect key={key} to={path} /> )

// export const redirectFrom = (from: string, to: string) => ({
//   path: from,
//   exact: true,
//   component: redirectTo(to),
// })

export const history = createBrowserHistory()