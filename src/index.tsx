import React from "react"
import ReactDom from "react-dom"
import { App } from "./App"
import { Router } from "react-router-dom";
import { history } from "./lib/routing"

const root = document.querySelector("#root")

const render = (): void => {
  ReactDom.render(
    <Router history={history}>
      <App />
    </Router>,
    root,
  )
}

if (module.hot) {
  module.hot.accept("./App", render)
}

render()