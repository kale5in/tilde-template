module.exports = {
    parser: "@typescript-eslint/parser",
    extends: [
        "plugin:@typescript-eslint/recommended",
        "react-app",
        "prettier",
        "prettier/@typescript-eslint",
    ],
    plugins: ['@typescript-eslint', 'react'],
    rules: {
      "@typescript-eslint/no-use-before-define": [ "error", { functions: false, classes: true, variables: false, } ],
      "@typescript-eslint/explicit-function-return-type": ["error", {
        "allowTypedFunctionExpressions": true,
        "allowExpressions": true 
      }],
      "indent": "off",
      "@typescript-eslint/indent": ["error", 2],
      "multiline": {
        "delimiter": "none"
      },
    },
  };